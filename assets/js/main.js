
var mensaje = swal.mixin({
	buttonsStyling : false,
	confirmButtonClass : "btn btn-info"
});

function transform_data(){

  var source = document.getElementById("source").value.trim();
  var source_machines = document.getElementById("machines").value.trim();
  var check_repeats = document.getElementById("delete_repeats");


  if(source.length < 1){

    mensaje({
    	title : "Debes de ingresar un contenido en Maquinas + Ip validos",
      type : "warning"
    });
    return;
  }

  if(source_machines.length < 1){

    mensaje({
    	title : "Debes de ingresar un contenido en Lista de maquinas validos",
      type : "warning"
    });
    return;
  }

  console.log(source);
  source = source.trim().split("\n");

  source_machines = source_machines.trim().split("\n");

  var list_machines_found = [];
  var list_machines_not_found = [];

  var output = source.map(function(current_val){

    current_val = current_val.split(" ");

    list_machines_found.push(current_val[3]);

    return current_val[3] + "\t" + current_val[4].replace("[","").replace("]","");
  });


  for(var i = 0; i < source_machines.length; i++){

    var founded = list_machines_found.filter(function(current_found){
      return current_found.search(new RegExp(source_machines[i],"gi")) != -1;
    });

    if(founded.length < 1){
      list_machines_not_found.push(source_machines[i]);
    }

  }


  if(check_repeats.checked == true){

    var unique_output = {};
    for(var i = 0; i < output.length; i++){
    	if(unique_output[output[i]] == undefined){
    		unique_output[output[i]] = output[i];
    	}
    }

    var unique_machines_not_found = {};
    for(var i = 0; i < list_machines_not_found.length; i++){
    	if(unique_output[list_machines_not_found[i]] == undefined){
    		unique_machines_not_found[list_machines_not_found[i]] = list_machines_not_found[i];
    	}
    }

    document.getElementById("output").value = Object.keys(unique_output).join("\n");
    document.getElementById("machines_not_found").value = Object.keys(unique_machines_not_found).join("\n");

  }else{

    document.getElementById("output").value = output.join("\n");
    document.getElementById("machines_not_found").value = list_machines_not_found.join("\n");
  }


  console.log(output);
};


window.addEventListener("load", function(){


  document.getElementById("btn_action").addEventListener("click", transform_data);
});
